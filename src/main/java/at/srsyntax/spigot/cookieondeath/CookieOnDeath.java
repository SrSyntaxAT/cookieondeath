package at.srsyntax.spigot.cookieondeath;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by SrSyntaxAT | Marcel H. on 01.08.2019 at 17:55. All rights reserved.
 */

public class CookieOnDeath extends JavaPlugin implements Listener {

    private ItemStack cookie;

    @Override
    public void onEnable() {
        getLogger().info("Plugin by SrSyntaxAT - Twitter, SpigotMC & GitLab: SrSyntaxAT");

        cookie = new ItemStack(Material.COOKIE);
        ItemMeta itemMeta = cookie.getItemMeta();
        itemMeta.setDisplayName("§bEhrenkeks");
        cookie.setItemMeta(itemMeta);

        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        getLogger().info("Plugin by SrSyntaxAT - Twitter, SpigotMC & GitLab: @SrSyntaxAT");
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Player opfer = event.getEntity();
        Player ehrenmann = opfer.getKiller();
        if (ehrenmann != null) {
            ehrenmann.sendMessage("§aDu Ehrenmann hast §e" + opfer.getName() + " §agetötet, daher bekommst du einen §e§lKeks§a.");
            ehrenmann.getInventory().addItem(cookie);
        }
    }
}
